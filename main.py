from BlockFollowers import BlockFollowers
import threading
import time
import wx

class MyFrame(wx.Frame):    
    def __init__(self):
        super().__init__(parent=None, title='Block Followers', size=wx.Size(600, 220))
        panel = wx.Panel(self)

        wx.StaticText(panel, label="Enter Twitter Users screen name without the @", pos=(5,5))

        self.text_ctrl = wx.TextCtrl(panel, pos=(5,25))

        self.my_btn = wx.Button(panel, label='Block Followers', pos=(5,50))
        self.my_btn.Bind(wx.EVT_BUTTON, self.on_press_block)  


        wx.StaticText(panel, label="Enter path to blocked user list 'C:\\folder\\block_USERNAME'", pos=(5,100))

        self.text_ctrl2 = wx.TextCtrl(panel, pos=(5,120))

        self.my_btn2 = wx.Button(panel, label='Unblock Followers', pos=(5,150))
        self.my_btn2.Bind(wx.EVT_BUTTON, self.on_press_unblock)      

        self.console_output = [
            wx.StaticText(panel, label="", pos=(350,5)),
            wx.StaticText(panel, label="", pos=(350,25)),
            wx.StaticText(panel, label="", pos=(350,45)),
            wx.StaticText(panel, label="", pos=(350,65)),
            wx.StaticText(panel, label="", pos=(350,85)),
            wx.StaticText(panel, label="", pos=(350,105))
        ]

        self.time_elapsed = wx.StaticText(panel, label="", pos=(350,130))
        self.time_remaining = wx.StaticText(panel, label="", pos=(350,150))

        self.Show()

    def on_press_block(self, event):
        value = self.text_ctrl.GetValue()
        if not value:
            self.print_to_screen("You didn't enter anything!")
        else:
            block = BlockFollowers(self)

            self.t = threading.Thread(target=block.block_all_followers_of, name="blocking", args=[value])
            self.t.daemon = True
            self.t.start()

            self.t2 = threading.Thread(target=self.update_time, name="time", args=[block])
            self.t2.daemon = True
            self.t2.start()

    def on_press_unblock(self, event):
        path = self.text_ctrl2.GetValue()
        if not path:
            self.print_to_screen("You didn't enter anything!")
        else:
            block = BlockFollowers(self)

            self.t = threading.Thread(target=block.unblock_all_followers, name="blocking", args=[path])
            self.t.daemon = True
            self.t.start()

            self.t2 = threading.Thread(target=self.update_time, name="time", args=[block])
            self.t2.daemon = True
            self.t2.start()


    def update_time(self, block):
        start_time = time.time()
        while True:
            if self.t.is_alive():
                elapsed_time = time.time() - start_time
                self.time_elapsed.SetLabel(self.get_formatted_time(elapsed_time) + " elapsed")
                if block.blocked_followers != 0:
                    seconds_remaining = (elapsed_time/block.blocked_followers)*(block.total_followers-block.blocked_followers)
                    self.time_remaining.SetLabel(self.get_formatted_time(seconds_remaining) + " remaining")
                
                time.sleep(1)
            else:
                break

    def get_formatted_time(self, seconds):
        hours = int(((seconds)/60)/60)
        mins = int((seconds/60)%60)
        secs = int(seconds%60) 
        return f"{hours}:{mins}:{secs}"

    def print_to_screen(self, text):
        new_line = text
        old_line = ""
        for line in self.console_output[::-1]:
            old_line = line.GetLabel()
            line.SetLabel(new_line)
            new_line = old_line
        print(text)

app = wx.App()
frame = MyFrame()
app.MainLoop()
