from requests_oauthlib import OAuth1Session
import socket
import webbrowser
import twitter
consumer_key = 'XXXXXXXXXXXXXXXXXXXXXXX'
consumer_secret = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

def get_resource_token():
    request_token = OAuth1Session(client_key=consumer_key, client_secret=consumer_secret)
    url = 'https://api.twitter.com/oauth/request_token'
    fetch_response = request_token.fetch_request_token(url)
    resource_owner_key = fetch_response["oauth_token"]
    resource_owner_secret = fetch_response["oauth_token_secret"]
    resource = [resource_owner_key, resource_owner_secret]
    return resource

def login():
    resource = get_resource_token()
    webbrowser.open(f"http://api.twitter.com/oauth/authenticate?oauth_token={resource[0]}")
    buffer = listen_for_response()
    verifier = buffer.decode().split("?")[1].split("&")[1].split(" ")[0].split("=")[1]
    access_token_list = twitter_get_access_token(verifier, resource[0], resource[1])
    access_token = access_token_list[0].split("=")[1]
    access_secret_token = access_token_list[1].split("=")[1]
    api = twitter.Api(consumer_key=consumer_key, consumer_secret=consumer_secret, access_token_key=access_token, access_token_secret=access_secret_token, sleep_on_rate_limit=True)
    user_data = twitter_get_user_data(access_token_list)
    return user_data["screen_name"], api

def listen_for_response():
    
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(('localhost', 8080))
    serversocket.listen(5)
    buf = None
    while True:
        connection, _ = serversocket.accept()
        buf = connection.recv(4096)
        if len(buf) > 0:
            break
    return buf

def twitter_get_user_data(access_token_list):
    access_token_key = str.split(access_token_list[0], '=')
    access_token_secret = str.split(access_token_list[1], '=')
    access_token_name = str.split(access_token_list[3], '=')
    access_token_id = str.split(access_token_list[2], '=')
    key = access_token_key[1]
    secret = access_token_secret[1]
    name = access_token_name[1]
    id = access_token_id[1]
    oauth_user = OAuth1Session(client_key=consumer_key,
                            client_secret=consumer_secret,
                            resource_owner_key=key,
                            resource_owner_secret=secret)
    url_user = 'https://api.twitter.com/1.1/account/verify_credentials.json'
    params = {}
    user_data = oauth_user.get(url_user, params=params)
    
    return user_data.json()

def twitter_get_access_token(verifier, ro_key, ro_secret):
    oauth_token = OAuth1Session(client_key=consumer_key,
                                client_secret=consumer_secret,
                                resource_owner_key=ro_key,
                                resource_owner_secret=ro_secret)
    url = 'https://api.twitter.com/oauth/access_token'
    data = {"oauth_verifier": verifier}
   
    access_token_data = oauth_token.post(url, data=data)
    access_token_list = str.split(access_token_data.text, '&')
    return access_token_list