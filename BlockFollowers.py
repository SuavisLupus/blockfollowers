import Login
import time

class BlockFollowers:

    console_out = ["", "", "", "", "", ""]

    def __init__(self, screen):
        self.screen = screen
        self.username, self.api = Login.login()
        self.blocked_followers = 0
        self.total_followers = 0

    def get_number_of_followers(self, screen_name):
        user = self.api.GetUser(screen_name=screen_name)
        return user.followers_count

    def block_all_followers_of(self, target_screen_name):
        try:
            self.total_followers = self.get_number_of_followers(target_screen_name)

            next_cursor = -1
            self.blocked_followers = 0
            f = open(f"block_{target_screen_name}", "w")
            f.close()
            while True:
                next_cursor, _, users = self.api.GetFollowersPaged(screen_name=target_screen_name, cursor=next_cursor, count=200)

                for user in users:
                    f = open(f"block_{target_screen_name}", "a")
                    self.api.CreateBlock(screen_name=user.screen_name)
                    f.write(user.screen_name + "\n")
                    self.blocked_followers += 1
                    self.screen.print_to_screen(f"Blocked {user.screen_name}")
                    self.screen.print_to_screen(f"{self.blocked_followers}/{self.total_followers} ({int((self.blocked_followers/self.total_followers)*10000)/100}%)")
                    f.close()
                if(len(users) == 0):
                    break
            self.screen.print_to_screen("All Done!")
        except Exception as E:
            self.screen.print_to_screen(str(E))


    def unblock_all_followers(self, list_name):
        f = open(list_name)
        for line in f:
            self.screen.print_to_screen("Unblocked: " + line[:-1])
            try:
                self.api.DestroyBlock(screen_name=line[:-1])
            except:
                self.screen.print_to_screen(f"could not find user {line[:-1]}")
        self.screen.print_to_screen("All Done!")

    def print_time_stats(self, seconds_past):
        self.screen.print_to_screen(self.get_formatted_time(seconds_past) + " elapsed")
        seconds_remaining = (seconds_past/self.blocked_followers)*(self.total_followers-self.blocked_followers)
        self.screen.print_to_screen(self.get_formatted_time(seconds_remaining) + " remaining")

    def get_formatted_time(self, seconds):
        hours = int(((seconds)/60)/60)
        mins = int((seconds/60)%60)
        secs = int(seconds%60) 
        return f"{hours}:{mins}:{secs}"



